<?php

if (!defined('PURPLE_NAME')){
    define('PURPLE_NAME',         'Purple Agency - Weather fetcher');
    define('PURPLE_CLASS_NAME',   'purple');
    define('PURPLE_VERSION',      '1.0');
}


return array(
    'author'         => 'Purple',
    'author_url'     => 'https://purple.agency/',
    'name'           => PURPLE_NAME,
    'description'    => 'Purple Agency - Weather fetcher',
    'version'        => PURPLE_VERSION,
    'namespace'      => 'Purple\Weather',
    'settings_exist' => true,
    'fieldtypes' => array(
        'editor' => array(
            'name' => 'Purple',
            'compatibility' => 'text'
        ),
    ),
    'models' => array(
        'Config'   => 'Model\Config',
    ),
   
    //----------------------------------------
    // Default Module Settings
    //----------------------------------------
    'settings_module' => array(),

    //----------------------------------------
    // Default Fieldtype Settings
    //----------------------------------------
    'settings_fieldtype' => array(
        'config' => null,
    ),

);

/* End of file addon.setup.php */
/* Location: ./system/user/addons/editor/addon.setup.php */