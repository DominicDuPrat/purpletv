<?php
/**
 *  Purple Agency  - weather fetch & cache
 *
 * @package  ExpressionEngine
 * @subpackage Plugins
 * @category Plugins
 * @author    Paul Carnell
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$plugin_info = array(
	'pi_name' => 'Purple Agency - Weather fetcher',
	'pi_version' => '1.0',
	'pi_author' => ' Paul Carnell',
	'pi_author_url' => 'https://purple.agency',
	'pi_description' => 'Purple Agency - Weather fetcher',
	'pi_usage' => Pp_weather::usage()
);


class Pp_weather
{
    // api town code
    private $api_town_code = '310025';

    // api key
    private $api_key = 'aaf805ef-d583-4a5d-800b-df8a78e05286';

    // api url, $s will be replaced with town code and api key
    private $api_url = 'http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/%s?res=3hourly&key=%s';

    // how often in minutes to update this feed
    private $frequency = 15;

    // not using
	var $return_data = "";

    // json feed data
    private $json = false;

    // where is the cache stores?
    private $cache_name = false;

    // what data is available? for reference only in the template use {F} fpr example to fet the feels like temp
    private $data = [
        "I" => "Icon code",
        "F" => "Feels Like Temperature", // c
        "Ff" => "Feels Like Temperature", // f
        "G" => "Wind Gust",
        "H" => "Screen Relative Humidity",
        "T" => "Temperature", //c
        "Tf" => "Temperature", //f
        "V" => "Visibility",
        "Vt" => "Visibility",
        "D" => "Wind Direction",
        "S" => "Wind Speed",
        "U" => "Max UV Index",
        "W" => "Weather Type",
        "Wt" => "Weather Type",
        "Pp" => "Precipitation Probability",
        "tN" => "Text now",
        "tL" => "Text later", // + 3 hours
    ];

    // visibility data
    private $visibility_codes = [
        'UN' => 'Unknown',
        'VP' => 'Very poor', // - Less than 1 km',
        'PO' => 'Poor', // - Between 1-4 km',
        'MO' => 'Moderate', // - Between 4-10 km',
        'GO' => 'Good', // - Between 10-20 km',
        'VG' => 'Very good', // - Between 20-40 km',
        'EX' => 'Excellent', // - More than 40 km',
    ];

    // weather codes
    private $weather_codes = [
        'NA' => ['Not available','wi-na'],
        '0' => ['Clear night','wi-night-clear'],
        '1' => ['Sunny day','wi-day-sunny'],
        '2' => ['Partly cloudy (night)','wi-night-alt-partly-cloudy'],
        '3' => ['Partly cloudy (day)','wi-day-sunny-overcast'],
        '4' => ['Not used','wi-na'],
        '5' => ['Mist','wi-day-fog'],
        '6' => ['Fog','wi-day-fog'],
        '7' => ['Cloudy','wi-day-cloudy'],
        '8' => ['Overcast','wi-cloudy'],
        '9' => ['Light rain shower (night)','wi-night-alt-showers'],
        '10' => ['Light rain shower (day)','wi-day-showers'],
        '11' => ['Drizzle','wi-day-showers'],
        '12' => ['Light rain','wi-day-showers'],
        '13' => ['Heavy rain shower (night)','wi-night-alt-rain'],
        '14' => ['Heavy rain shower (day)','wi-day-rain'],
        '15' => ['Heavy rain','wi-rain'],
        '16' => ['Sleet shower (night)','wi-night-sleet'],
        '17' => ['Sleet shower (day)','wi-day-sleet'],
        '18' => ['Sleet','wi-sleet'],
        '19' => ['Hail shower (night)','wi-night-hail'],
        '20' => ['Hail shower (day)','wi-day-hail'],
        '21' => ['Hail','wi-hail'],
        '22' => ['Light snow shower (night)','wi-night-snow'],
        '23' => ['Light snow shower (day)','wi-day-snow'],
        '24' => ['Light snow','wi-snow'],
        '25' => ['Heavy snow shower (night)','wi-night-snow'],
        '26' => ['Heavy snow shower (day)','wi-day-snow'],
        '27' => ['Heavy snow','wi-snow'],
        '28' => ['Thunder shower (night)','wi-night-alt-thunderstorm'],
        '29' => ['Thunder shower (day)','wi-day-thunderstorm'],
        '30' => ['Thunder','wi-thunderstorm'],
    ];

    /**
     * Pp_weather constructor.
     */
    function __construct()
    {
		
        //$this->set_cache_name();
       // $this->check_cache();	
    }

    /**
     * Execute a template update
     * @return mixed
     */
    function exec()
    {
		
		$this->get_weather();
        // get tag data
        $template = ee()->TMPL->tagdata;

        // json feed returns 5 days worth of data split into 3 hours per day
        // we will just use the latest data, HOWEVER we may want to change these from
        // period[0] and period[1] to period[1] and period[2] as the data could be 2 hours 45mins old

		
		$hour = date('H');

        // Current
        $source_now = $this->json['SiteRep']['DV']['Location']['Period'][0]['Rep'][1];
		

		 // + 3 hours, can change the period to 2/3/4/5 each period should be 3 hours
        $source_later = $this->json['SiteRep']['DV']['Location']['Period'][2]['Rep'][0];
		

        // slide in our custom bits, weather text and visibility text
        $source_now['I'] = $this->weather_codes[$source_now['W']][1];
       // $source_now['Wt'] = $this->weather_codes[$source_now['W']][0];
       // $source_now['Vt'] = $this->visibility_codes[$source_now['V']];
        $source_now['tN'] = $this->trim($this->weather_codes[$source_now['W']][0]);
        //$source_now['tL'] = $this->trim($this->weather_codes[$source_later['W']][0]);
       // $source_now['Ff'] = $this->celsius_to_fahrenheit($source_now['F']);
        $source_now['Tf'] = $this->celsius_to_fahrenheit($source_now['T']);

        $search = [];
        $replace = [];
        foreach($this->data as $id=>$value)
        {
            $search[] = '{' . $id . '}';
            $replace[] = isset($source_now[$id]) ? $source_now[$id] : '';
        }

        return str_replace($search, $replace, $template);
    }

    /**
     * Convert from C to F
     * @param $value
     * @return string
     */
    private function celsius_to_fahrenheit($value)
    {
        return number_format($value * 9 / 5 + 32, 1);
    }

    /**
     * Remove N
     * @param $what
     * @return string
     */
    private function trim($what)
    {
        $result = str_replace('(day)', '', $what);
        $result = str_replace('(night)', '', $result);
        return trim($result);
    }
	
	private function get_weather(){

		$filename = sprintf($this->api_url, $this->api_town_code, $this->api_key);						
        ini_set('default_socket_timeout', 5); // timeout after 5 seconds
        $file = file_get_contents($filename);		
		$this->json = json_decode($file, true);		
	}
	
    /**
     * Use the EE cache folder as a place to store out json
     */
    private function set_cache_name()
    {
        // copied from EE Output.php to get cache path
        $path = ee()->config->item('cache_name');		
        $cache_name = ($path == '') ? APPPATH.'cache/' : $path;
				
        if ( ! is_dir($cache_name) || ! is_really_writable($cache_name))
        {
            log_message('error', "Unable to write cache file: ".$cache_name);
            return;
        }

        $this->cache_name = $cache_name . 'weather.json';
    }

    /**
     * Check cache date, if older than 15 minutes refresh it
     * Also loads the cache which is a json feed
     */
    private function check_cache()
    {
        // file doesn't exist or older than 15 minutes
        if ( ( ! file_exists($this->cache_name) || (filemtime($this->cache_name) < strtotime('-' . $this->frequency . ' minutes')))) $this->update_cache();

        // load file
        $this->load_cache();
    }

    /**
     * Makes the API call and updates/refreshes the cache data
     */
    private function update_cache()
    {
        // This may need to be replaced with a curl call
        $filename = sprintf($this->api_url, $this->api_town_code, $this->api_key);		
        ini_set('default_socket_timeout', 5); // timeout after 5 seconds
        $file = file_get_contents($filename);

        // save
        //if ($file) file_put_contents($this->cache_name, $file);

        return true;
    }

    /**
     * Load the file as JSON and save as an array
     */
    private function load_cache()
    {
        // load the file from disk
        $file = file_get_contents($this->cache_name);
        if ( ! $file) {
            error_log('PP Weather - Error reading cache file '.$this->cache_name);
            return false;
        }

        // decode it into an array
        $this->json = json_decode($file, true);

        return true;
    }

    /**
     * Plugin usage for dashboard
     * @return string
     */
    function usage()
	{
		ob_start();
?>
Place the following tag in your header or footer, edit the template as required;
{exp:pp_weather:exec}
        Weather Icon : {I}<br>
        Feels Like Temperature : {F} (C)<br>
        Feels Like Temperature : {Ff} (F)<br>
        Wind Gust : {G}<br>
        Screen Relative Humidity : {H}<br>
        Temperature : {T} (C)<br>
        Temperature : {Tf} (F)<br>
        Visibility : {V} {Vt}<br>
        Wind Direction : {D}<br>
        Wind Speed : {S}<br>
        Max UV Index : {U}<br>
        Weather Type : {W} {Wt}<br>
        Precipitation Probability : {Pp}<br>
        Text Now : {tN}<br>
        Text later : {tL}<br>
{/exp:pp_weather:exec}
<?php
		$buffer = ob_get_contents();

		ob_end_clean();

		return $buffer;
	}

}