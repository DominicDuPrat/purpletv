<?php
/**
 *  Purple Agency  - weather fetch & cache
 *
 * @package  ExpressionEngine
 * @subpackage Plugins
 * @category Plugins
 * @author    Paul Carnell
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$plugin_info = array(
	'pi_name' => 'Purple Agency - Train Departures',
	'pi_version' => '1.0',
	'pi_author' => ' Paul Carnell',
	'pi_author_url' => 'https://purple.agency',
	'pi_description' => 'Purple Agency - Train Departures',
	'pi_usage' => Pp_railway::usage()
);

class Pp_railway
{
	private $departures = "https://huxley.apphb.com/departures/bsk?accessToken=9f79dcf6-8680-4282-881c-8dd49c43fd47";
	
	private  $arrivals = "https://huxley.apphb.com/arrivals/bsk?accessToken=9f79dcf6-8680-4282-881c-8dd49c43fd47";

    /**
     * Pp_weather constructor.
     */
    function __construct()
    {
		
    }

    /**
     * Execute a template update
     * @return mixed
     */
    function arr()
    {
        $json = file_get_contents($this->arrivals);
		$obj = json_decode($json);				
		$times  = array();
		$i = 0;		
		foreach ($obj->trainServices as $dest){			
			$times[$i]['origin'] = $dest->origin[0]->locationName;			
			$times[$i]['destination'] = $dest->origin[0]->locationName;
			$times[$i]['due'] = $dest->sta;
			$times[$i]['expected'] = $dest->eta;
			//$i++;			
			if ($i++ == 3) break;
		}
		
		$data['times'] = $times;		
        return (ee()->load->view('table',$data,true));
    }
	
	 function dest()
    {
        $json = file_get_contents($this->departures);
		$obj = json_decode($json);				
		$times  = array();
		$i = 0;		
		foreach ($obj->trainServices as $dest){			
			$times[$i]['origin'] = $dest->origin[0]->locationName;			
			$times[$i]['destination'] = $dest->destination[0]->locationName;
			$times[$i]['due'] = $dest->std;
			$times[$i]['expected'] = $dest->etd;
			//$i++;			
			if ($i++ == 3) break;
		}
		
		$data['times'] = $times;		
        return (ee()->load->view('table',$data,true));
    }
	
	function get_m4()
    {
		$rss = new DOMDocument();
		$rss->load('http://m.highways.gov.uk/feeds/rss/UnplannedEvents/M4.xml');
		
		$feed = array();
			foreach ($rss->getElementsByTagName('item') as $node) {
				$item = array ( 
				'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
				'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue
				);
				array_push($feed, $item);
			}
		if (count($feed) > 0 ) {
			
			$data['motorway'] = 'M4';		
			$data['incidents'] = $feed;		
			return (ee()->load->view('feed',$data,true));
		} else {
			
			$data['motorway'] = 'M4';		
			$data['incidents'] = 'No Reported Incidents';		
			return (ee()->load->view('no-feed',$data,true));			
		}
		
	}
	
	function get_m3()
    {
		$rss = new DOMDocument();
		$rss->load('http://m.highways.gov.uk/feeds/rss/UnplannedEvents/M3.xml');
		
		$feed = array();
			foreach ($rss->getElementsByTagName('item') as $node) {
				$item = array ( 
				'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
				'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue
				);
				array_push($feed, $item);
			}
			
		if (count($feed) > 0 ) {
			
			$data['motorway'] = 'M3';		
			$data['incidents'] = $feed;		
			return (ee()->load->view('feed',$data,true));
		} else {
			
			$data['motorway'] = 'M3';		
			$data['incidents'] = 'No Reported Incidents';		
			return (ee()->load->view('no-feed',$data,true));			
		}
		
	}
    /**
     * Plugin usage for dashboard
     * @return string
     */
    function usage()
	{
		ob_start();
?>
Place the following tag in your header or footer, edit the template as required;
{exp:pp_weather:exec}
        Weather Icon : {I}<br>
        Feels Like Temperature : {F} (C)<br>
        Feels Like Temperature : {Ff} (F)<br>
        Wind Gust : {G}<br>
        Screen Relative Humidity : {H}<br>
        Temperature : {T} (C)<br>
        Temperature : {Tf} (F)<br>
        Visibility : {V} {Vt}<br>
        Wind Direction : {D}<br>
        Wind Speed : {S}<br>
        Max UV Index : {U}<br>
        Weather Type : {W} {Wt}<br>
        Precipitation Probability : {Pp}<br>
        Text Now : {tN}<br>
        Text later : {tL}<br>
{/exp:pp_weather:exec}
<?php
		$buffer = ob_get_contents();

		ob_end_clean();

		return $buffer;
	}

}