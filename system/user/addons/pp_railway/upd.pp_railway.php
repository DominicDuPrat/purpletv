<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adare_mail_list_upd{
	public $version = '1.5';

    private $module_name = "Adare_mail_list";
	 // Constructor
    public function __construct()
    {
        $this->EE =& get_instance();
    }

	function install()
	{
		$data = array(
			'module_name' => $this->module_name,
			'module_version' => $this->version,
			'has_cp_backend' => 'n',
			'has_publish_fields' => 'n'
		);

		$this->EE->db->insert('modules', $data);

		$data = array(
			'class'     => 'Adare_mail_list' ,
			'method'    => 'update_list'
		);

		ee()->db->insert('actions', $data);
		

		unset($fields);

		return TRUE;
	}


	 public function update($current = '')
    {
        if ($current == $this->version) {
            // No updates
            return FALSE;
        }

		$data = array(
			'class'     => 'Adare_mail_list' ,
			'method'    => 'remove_from_list'
		);

		ee()->db->insert('actions', $data);	
		
        return TRUE;
    }

	function uninstall()
	{
		$this->EE->load->dbforge();

		$this->EE->db->select('module_id');
		$query = ee()->db->get_where('modules', array('module_name' => 'Adare_mail_list'));

		$this->EE->db->where('module_id', $query->row('module_id'));
		$this->EE->db->delete('module_member_groups');

		$this->EE->db->where('module_name', 'Adare_mail_list');
		$this->EE->db->delete('modules');

		$this->EE->db->where('class', 'Adare_mail_list');
		$this->EE->db->delete('actions');

		$this->EE->dbforge->drop_table('adare_mail_list');

		return TRUE;
	}
}
