// Jqueries go here

$(document).ready(function() {
    // Window height
   
	var video = document.getElementById("video");
	
    $('.flexslider').flexslider({
        controlNav:  false,
        directionNav: false,
		controlNav: false,
		slideshowSpeed: 10000,
		after: function(){
        // sets active_id to the active slide
        var active_id = $('.flex-active-slide').attr('id');
        //if the active slide is the video slide...  
        if( active_id == "video-slide"){
          //play the video and pause the slider
         video.play();		
        $('.flexslider').flexslider("pause");
          //when the video has ended, go to the next slide and play the slider
          video.onended = function(){
              $('.flexslider').flexslider("next");
              $('.flexslider').flexslider("play");
			  video.play();		
          }
        }
		 if( active_id == "traffic-slide"){         
		  //load divs whilst video playing
		  	$("#road-m3").load(location.href + " #road-m3");
			$("#road-m4").load(location.href + " #road-m4");
			$("#rail-dept").load(location.href + " #rail-dept");
			$("#rail-arr").load(location.href + " #rail-arr");
			$("#sidebar-weather").load(location.href + " #sidebar-weather");
			//$("#sidebar-news").load(location.href + " #sidebar-news");        
        }
    },
    });
    
    $(".scale-with-window").each(function() {
        var parentWidth = $(this).parent().width();
        var imageWidth = $(this).width();
        if (imageWidth > parentWidth) {
            $(this).css({
                left: (parentWidth - imageWidth) / 2
            });
        } else {
            
        }
        
    });
    
     $(window).resize(function() {
         $(".scale-with-window").each(function() {
            var parentWidth = $(this).parent().width();
            var imageWidth = $(this).width();
            if (imageWidth > parentWidth) {
                $(this).css({
                    left: (parentWidth - imageWidth) / 2
                });
            } else {

            }
        });
     });
	 
});
    

	/*
window.setInterval(function(){
	
	$("#mydiv").load(location.href + " #road-m3");
	$("#mydiv").load(location.href + " #road-m4");
	$("#mydiv").load(location.href + " #rail-dest");
	$("#mydiv").load(location.href + " #rail-arr");
	$("#mydiv").load(location.href + " #sidebar-weather");
	$("#mydiv").load(location.href + " #sidebar-news");
	
  /// 5 minute update
}, 300000);	


window.setInterval(function(){	
	$("#mydiv").load(location.href + " #sidebar-weather");	
  /// 5 minute update
}, 3600000 );	
*/
	